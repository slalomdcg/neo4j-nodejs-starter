# Installing Node
If you don't already have Node, run the following commands in shell. This will install Node Version Manager, then the latest stable version Node.
```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.1/install.sh | bash
nvm install stable
```
