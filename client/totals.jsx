import React from 'react';
import qwest from 'qwest';

function loadData(that) {
    qwest.get('/totals/nodes', undefined, {type: 'json'}).then((xhr, data) => {
        that.setState({
            nodes: data
        });
    });

    qwest.get('/totals/nodes?type=Route53', undefined, {type: 'json'}).then((xhr, data) => {
        that.setState({
            route53: data
        });
    });

    qwest.get('/totals/nodes?type=ELB', undefined, {type: 'json'}).then((xhr, data) => {
        that.setState({
            elb: data
        });
    });

    qwest.get('/totals/nodes?type=EC2', undefined, {type: 'json'}).then((xhr, data) => {
        that.setState({
            ec2: data
        });
    });

    qwest.get('/totals/nodes?type=DynamoDB', undefined, {type: 'json'}).then((xhr, data) => {
        that.setState({
            dynamo: data
        });
    });

    qwest.get('/infrastructure', undefined, {
        type: 'json'
    }).then((xhr, data) => {
        that.setState({
            nodeData: data.nodes
        });
    });
}

function renderDropDown(nodes) {
    if (nodes) {
        return nodes.map((node, i) => {
            return <option key={i} value={node.name}>{node.name}</option>;
        });
    }
}

export default React.createClass({
    getInitialState() {
        return {}
    },

    componentDidMount() {
        loadData(this);
    },

    componentDidUpdate(prevProps) {
        if (this.props.changed !== prevProps.changed) {
            loadData(this);
        }
    },

    deleteNode() {
        let name = this.refs.delete.value;

        qwest.delete(`/delete?name=${name}`, undefined, {type: 'json'}).then(() => {
            this.props.onChange();
        });
    },

    addNode() {
        let name = this.refs.nodeName.value,
            type = this.refs.nodeType.value;

        qwest.post(`/node?name=${name}&type=${type}`, undefined, {type: 'json'}).then(() => {
            this.props.onChange();
        });
    },

    connectNodes() {
        let name1 = this.refs.fromNode.value,
            name2 = this.refs.toNode.value;

        qwest.post(`/connect?name1=${name1}&name2=${name2}`, undefined, {type: 'json'}).then(() => {
            this.props.onChange();
        });
    },

    render() {
        return (
            <section className="totals">
                <div>Total number of nodes: {this.state.nodes}</div>
                <div>Total number of Route53s: {this.state.route53}</div>
                <div>Total number of ELBs: {this.state.elb}</div>
                <div>Total number of EC2s: {this.state.ec2}</div>
                <div>Total number of DynamoDBs: {this.state.dynamo}</div>

                <div className="input-node add-node">
                    <label htmlFor="add-node">Add node:</label>
                    <input id="add-node" ref="nodeName"/>
                    <select ref="nodeType">
                        <option value="Route53">Route53</option>
                        <option value="ELB">ELB</option>
                        <option value="EC2">EC2</option>
                        <option value="DynamoDB">DynamoDB</option>
                    </select>
                    <button type="button" onClick={this.addNode}>Add</button>
                </div>

                <div className="input-node connect-nodes">
                    <span>
                        <label htmlFor="connect-nodes">Connect :</label>
                        <select ref="fromNode">
                            {renderDropDown(this.state.nodeData)}
                        </select>
                    </span>
                    <span>
                        <label htmlFor="target-node"> to :</label>
                        <select ref="toNode">
                            {renderDropDown(this.state.nodeData)}
                        </select>
                        <button type="button" onClick={this.connectNodes}>OK</button>
                    </span>
                </div>


                <div className="input-node delete">
                    <label htmlFor="delete">Delete node:</label>
                    <input id="delete" ref="delete"/>
                    <button type="button" onClick={this.deleteNode}>Delete</button>
                </div>

            </section>
        );
    }
});