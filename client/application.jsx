/* global document */
import React from 'react';
import { render } from 'react-dom';

import Infrastructure from './infrastructure.jsx';
import Totals from './totals.jsx';

const node = document.getElementById('root');

const Application = React.createClass({
    getInitialState() {
        return {
            change: false
        }
    },

    onChange() {
        this.setState({
            change: !this.state.change
        });
    },

    render() {
        return (
            <div>
                <Infrastructure
                    changed={this.state.change}
                />
                <Totals
                    onChange={this.onChange}
                    changed={this.state.change}
                />
            </div>
        );
    }
});

render(<Application/>, node);
