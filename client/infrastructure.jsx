import React from 'react';
import qwest from 'qwest';

require('gojs');

let $ = go.GraphObject.make;

function createDiagram() {
    let diagram = $(go.Diagram, "infrastructure", {
        initialContentAlignment: go.Spot.Center, // center Diagram contents
        "undoManager.isEnabled": true, // enable Ctrl-Z to undo and Ctrl-Y to redo
        layout: $(
            go.LayeredDigraphLayout, // specify a Diagram.layout that arranges trees
            // {angle: 90, layerSpacing: 35}
            {
                direction: 90,
                layerSpacing: 10,
                columnSpacing: 15,
                setsPortSpots: false
            }
        )
    });

    diagram.linkTemplate = $(go.Link,
        // default routing is go.Link.Normal
        // default corner is 0
        {curve: go.Link.Bezier, corner: 5},
        $(go.Shape, {strokeWidth: 3, stroke: "#555"}) // the link shape

        // if we wanted an arrowhead we would also add another Shape with toArrow defined:
        // $(go.Shape, { toArrow: "Standard", stroke: null }
    );

    // the template we defined earlier
    diagram.nodeTemplate = $(go.Node, "Vertical",
        $(go.Picture,
            {margin: 10, width: 50, height: 50},
            new go.Binding("source")),
        $(go.TextBlock, "Default Text",
            {margin: 12, stroke: "black", font: "bold 16px sans-serif"},
            new go.Binding("text", "name"))
    );

    return diagram;
}

function renderDiagram() {
    qwest.get('/infrastructure', undefined, {
        type: 'json'
    }).then((xhr, data) => {
        this.state.diagram.model = new go.GraphLinksModel(
            data.nodes,
            data.relationships
        );
    });
}

export default React.createClass({
    getInitialState() {
        return {}
    },

    componentDidMount() {
        this.setState({
            diagram: createDiagram()
        });

        renderDiagram.call(this);
    },

    componentDidUpdate() {
        renderDiagram.call(this);
    },

    render() {
        return <section id="infrastructure"/>;
    }
});