/* eslint no-console:0 */
/* globals console */
import Hapi from 'hapi';
import Inert from 'inert';

import routes from './routes/index';

import { DEFAULT_PORT } from './config';

const server = new Hapi.Server();

server.connection({
  port: process.env.PORT || DEFAULT_PORT,
  host: '0.0.0.0'
});

server.register(Inert, () => {});
server.route(routes);

server.start(() => {
  console.log(`Running at ${server.info.uri}`);
});
