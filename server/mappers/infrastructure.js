import _ from 'lodash';

function toUI(node) {
    return {
        key: node._id,
        name: node.properties.name,
        source: 'imgs/' + node.labels + '.png'
    };
}
function toDiagramNode(infra) {
    return toUI(_.values(infra)[0]);
}

function toSingleDiagramNode(infra) {
    return toUI(_.values(infra)[2]);
}

function toDiagramRelationship(infra) {
    let rel = _.values(infra)[1];
    return {
        from: rel._fromId,
        to: rel._toId
    };
}

export default {
    toDiagram(infrastructure) {
        return {
            nodes: _.uniqBy(infrastructure.map(toDiagramNode).concat(infrastructure.map(toSingleDiagramNode)), 'key'),
            relationships: _.uniqWith(infrastructure.map(toDiagramRelationship), _.isEqual)
        };
    }
};