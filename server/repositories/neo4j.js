import neo4j from 'neo4j';

let db = new neo4j.GraphDatabase({
    url: 'http://neo4j:test@localhost:7474'
});

export default {
    getInfrastructure(callback) {
        db.cypher({
            query: 'MATCH (a), (node)-[rel]-() RETURN node, rel, a'
        }, callback);
    },

    getTotalNumberOfNodes(callback) {
        db.cypher({
            query: 'MATCH (n) return count(n)'
        }, callback);
    },

    getTotalNumberOfNodesByType(type, callback) {
        db.cypher({
            query: `MATCH (n:${type}) return count(n)`
        }, callback);
    },

    deleteByName(name, callback) {
        db.cypher({
            query: 'MATCH (n { name: {name} } ) detach delete n',
            params: {
                name: name
            }
        }, callback);
    },

    add(name, type, callback) {
        db.cypher({
            query: `create (n:${type} { name: {name} })`,
            params: {
                name: name
            }
        }, callback);
    },

    connect(node1Name, node2Name, callback) {
        db.cypher({
            query: 'MATCH (n1 { name: {node1Name} } ) ' +
                    'MATCH (n2 { name: {node2Name} } ) ' +
                    'create (n1)-[:CONNECTS_TO]->(n2)',
            params: {
                node1Name: node1Name,
                node2Name: node2Name
            }
        }, callback);
    }
};