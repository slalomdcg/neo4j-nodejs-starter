import controllers from '../controllers/index.js'

export default [
  { 
    path: '/favicon.ico',
    method: 'GET',
    handler: { file: '../favicon.ico' }
  },
  {
    path: '/{param*}',
    method: 'GET',
    handler: {
      directory: {
        path: 'assets',
        index: ['index.html']
      }
    }
  }
].concat(controllers);
