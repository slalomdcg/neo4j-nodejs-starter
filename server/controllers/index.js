import neo4j from '../repositories/neo4j';
import mapper from '../mappers/infrastructure';
import _ from 'lodash';

export default [
    {
        path: '/infrastructure',
        method: 'GET',
        handler: (request, reply) => {
            neo4j.getInfrastructure((err, infra) => {
                reply(mapper.toDiagram(infra));
            });
        }
    },
    {
        path: '/totals/nodes',
        method: 'GET',
        handler: (request, reply) => {
            if (!request.query.type) {
                neo4j.getTotalNumberOfNodes((err, count) => {
                    reply(_.values(count[0])[0]);
                });
            } else {
                neo4j.getTotalNumberOfNodesByType(request.query.type, (err, count) => {
                    reply(_.values(count[0])[0]);
                });
            }
        }
    },
    {
        path: '/delete',
        method: 'DELETE',
        handler: (request, reply) => {
            neo4j.deleteByName(request.query.name, () => {
                reply();
            });
        }
    },
    {
        path: '/node',
        method: 'POST',
        handler: (request, reply) => {
            neo4j.add(request.query.name, request.query.type,() => {
                reply();
            });
        }
    },
    {
        path: '/connect',
        method: 'POST',
        handler: (request, reply) => {
            neo4j.connect(request.query.name1, request.query.name2, (err) => {
                reply();
            });
        }
    }
];