#!/bin/bash

echo
echo "Installing Java..."
cd /usr/local
wget -nv -O jre-7u45-linux-x64.gz http://javadl.sun.com/webapps/download/AutoDL?BundleId=81812
tar -xf jre-7u45-linux-x64.gz
rm jre-7u45-linux-x64.gz
ln -s /usr/local/jre1.7.0_45/bin/java /usr/bin/java

echo
echo "Installing Neo4j..."
cd /etc
wget -nv http://dist.neo4j.org/neo4j-community-2.2.5-unix.tar.gz
tar -xf neo4j-community-2.2.5-unix.tar.gz
rm neo4j-community-2.2.5-unix.tar.gz
ln -s /etc/neo4j-community-2.2.5/bin/neo4j /usr/bin/neo4j
ln -s /etc/neo4j-community-2.2.5/bin/neo4j-shell /usr/bin/neo4j-shell
ln -s /etc/neo4j-community-2.2.5/bin/neo4j-import /usr/bin/neo4j-import

echo
echo "Updating Neo4j Config..."
sed -i 's/#org\.neo4j\.server\.webserver\.address=0\.0\.0\.0/org.neo4j.server.webserver.address=0.0.0.0/' /etc/neo4j-community-2.2.5/conf/neo4j-server.properties

yum install lsof -y

echo
echo "Starting Neo4j..."
neo4j start

sudo neo4j-import -into /etc/neo4j-community-2.2.5/data/workshop.db -nodes /tmp/nodes.csv -relationships /tmp/relationships.csv

sed -i 's/graph.db/workshop.db/g' /etc/neo4j-community-2.2.5/conf/neo4j-server.properties

neo4j restart

echo "Database now viewable at http://localhost:7474/browser/"